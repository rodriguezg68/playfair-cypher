# Development Setup

This project was develop and tested with Python 3.6. While it may be possible
to run with previous versions, it will work best with Python 3.6+

There is a requirements file that lists some helpful python packages used for
development:
* Pylint
  * Linting tool for Python that ensure files adhere to a common standard
    (PEP8)
* Black
  * Autoformatter for Python

To install, simply run:
```sh
pip install -r dev_requirements.txt
```

# Running scripts

Besides Python 3, nothing else should be required to run the scripts. If there
is a `python3` executable in your path, you should just be able to run:

```sh
./test.py
```

Since `python3` is not a valid alias on Windows, just running `python` should be
sufficient:

```sh
python ./test.py
```
