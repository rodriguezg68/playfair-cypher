#!/usr/bin/env python3
"""test
Simple testing script for the playfair class
"""
from playfair import Playfair, check_key, regular_key, spiral_key


def main():
    """Entry Point"""
    playfirst = Playfair("playfirstuvwxzbcdeghkmnoq")
    playexample = Playfair("playfairexample")

    # Encryption tests
    ciphertext = playfirst.encrypt("aabbbccc")
    assert ciphertext == "sefxvzvhevev"

    ciphertext = playfirst.encrypt("The quick brown fox jumped over the lazy dog")
    assert ciphertext == "ughnirkpwumzqanzriklgekzdsugdayxlgyo"

    ciphertext = playfirst.encrypt(
        "Advance right flank to Bunker Hill,"
        " then take up positions ready for attack."
    )
    assert (
        ciphertext == "lexpkedstcgupasaoiqzsqncudrpyrcgospnh"
        "savyktrurqotsnsglyqslszsykp"
    )

    ciphertext = playexample.encrypt("Hide the gold in the tree stump")
    assert ciphertext == "bmodzbxdnabekudmuixmmouvif"

    # Decryption tests
    plaintext = playfirst.decrypt("sefxvzvhevev")
    assert plaintext == "axabbxbccxcx"

    plaintext = playfirst.decrypt("ughnirkpwumzqanzriklgekzdsugdayxlgyo")
    assert plaintext == "thequickbrownfoxiumpedoverthelazydog"

    plaintext = playfirst.decrypt(
        "lexpkedstcgupasaoiqzsqncudrpyrcgospnhsavyktrurqotsnsglyqslszsykp"
    )
    assert (
        plaintext == "advancerightflanktobunkerhillthentakeupxpositionsreadyforatxtack"
    )

    plaintext = playexample.decrypt("bmodzbxdnabekudmuixmmouvif")
    assert plaintext == "hidethegoldinthetrexestump"

    # Check possible errors
    try:
        check_key(4, "clockwise", "standard")
    except SystemExit as exp:
        assert exp.code == 3

    try:
        check_key(0, "up", "standard")
    except SystemExit as exp:
        assert exp.code == 3

    try:
        check_key(2, "clockwise", "random")
    except SystemExit as exp:
        assert exp.code == 3

    try:
        regular_key("filler", 3, "clockwise", "standard")
    except SystemExit as exp:
        assert exp.code == 4

    try:
        spiral_key("filler", 3, "row", "standard")
    except SystemExit as exp:
        assert exp.code == 4

    print("Done!")


if __name__ == "__main__":
    main()
