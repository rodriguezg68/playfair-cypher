"""playfair
File that has class to setup Playfair encryption/decryption
"""
import string, sys


def fill_row(square, coords, letters, direction, row=5):
    """Function to fill in a row in the key square

    Arguments:
        square : dict
            Key square that we are updating to fill up a certain row
        coords : list
            Starting coordinate within the key square for the row
        letters : str
            A string of letters we iterate through to fill the key square
        direction: int
            The direction we are iterating through the row
        row : int, optional
            Length of the row we are filling (default: 5)

    Returns:
        index : int
            How far into the letters string we iterate through
    """
    index = 0
    while True:
        square[coords[0]][coords[1]] = letters[index]
        index += 1
        coords[1] += direction
        # Check if the y axis is an invalid index
        # If so, decrement it and break out of the loop
        if coords[1] in (-1, row):
            coords[1] -= direction
            break
        # Check if the coordinate in the key square is already filled
        # If so, decrement it and break out of the loop
        if square[coords[0]][coords[1]]:
            coords[1] -= direction
            break

    return index


def fill_col(square, coords, letters, direction, col=5):
    """Function to fill in a column in the key square

    Arguments:
        square : dict
            Key square that we are updating to fill up a certain row
        coords : list
            Starting coordinate within the key square for the row
        letters : str
            A string of letters we iterate through to fill the key square
        direction: int
            The direction we are iterating through the row
        col : int, optional
            Length of the row we are filling (default: 5)

    Returns:
        index : int
            How far into the letters string we iterate through
    """
    index = 0
    while True:
        square[coords[0]][coords[1]] = letters[index]
        index += 1
        coords[0] += direction
        # Check if the x axis is an invalid index
        # If so, decrement it and break out of the loop
        if coords[0] in (-1, col):
            coords[0] -= direction
            break
        # Check if the coordinate in the key square is already filled
        # If so, decrement it and break out of the loop
        if square[coords[0]][coords[1]]:
            coords[0] -= direction
            break

    return index


def get_padding(phrase, pad):
    """Get the filler letters to pad out the key"""
    # Determine how we are padding the key square
    if pad == "standard":
        # Standard fill pads out the rest of the alphabet starting with "A"
        remaining = string.ascii_lowercase
    else:
        # The other fill pattern starts after the last letter of the key phrase
        index = string.ascii_lowercase.index(phrase[-1])
        remaining = string.ascii_lowercase[index:] + string.ascii_lowercase[:index]

    pad_letters = "".join([x for x in remaining if x not in phrase and x != "j"])

    return pad_letters


def get_vertex(point1, point2, row, col):
    """Get the vertex in a rectangle

    Get row for first point and column for the second point and find the
    intersection to determine the rectangle vertex in the key square. Vertex
    will be relative to the first point.
    """
    col_list = {x for x in range(row * col) if x % col == point2 % col}
    row_list = {x for x in range(row * col) if x // row == point1 // row}

    # Sets don't support indexing, so transform it to a list first
    return list(row_list & col_list)[0]


def check_key(start, variant, pad):
    """Simple function to validate key arguments"""
    valid_start = (0, 1, 2, 3)
    if start not in valid_start:
        sys.stderr.write(
            "Error: {} is not a valid starting position for the key square.\n".format(
                start
            )
        )
        sys.stderr.write(
            "Error: Start position must be one of the following:\n{}\n".format(
                valid_start
            )
        )
        sys.exit(3)

    valid_variant = ("row", "column", "clockwise", "counter")
    if variant not in valid_variant:
        sys.stderr.write(
            "Error: {} is not a valid variant to fill the key square\n".format(variant)
        )
        sys.stderr.write(
            "Error: Variation must be one of the following:\n{}\n".format(valid_variant)
        )
        sys.exit(3)

    valid_pad = ("standard", "remainder")
    if pad not in valid_pad:
        sys.stderr.write(
            "Error: {} is not a valid method to pad the keyword\n".format(pad)
        )
        sys.stderr.write(
            "Error: Padding must be one of the following:\n{}\n".format(valid_pad)
        )
        sys.exit(3)


def setup_keyword(keyword):
    """Simple function to setup keyword"""
    letter_list = []
    new_keyword = keyword.replace(" ", "").lower()

    # If the key square is 5x5, we replace "j" with "i" for simplicity
    new_keyword = new_keyword.replace("j", "i")

    # Ensure that there are no duplicate letters in the keyword
    for letter in new_keyword:
        count = new_keyword.count(letter)
        if count == 1:
            letter_list.append(letter)
        elif count > 1 and letter not in letter_list:
            letter_list.append(letter)
        else:
            continue

    return "".join(letter_list)


def spiral_key(phrase, start, variant="clockwise", pad="standard"):
    """Creates a key in a spiral shape

    Arguments:
        keyword : str
            Keyword or keyphrase to use in the key square
        start : int, optional
            The starting position in the key square (default: 0)
            Supported values are 0, 1, 2, and 3. These correspond to the
            four corners of the key square.  0 refers to the top left
            corner, 1 refers to the top right corner, 2 refers to the
            bottom right corner, and 3 refers to the bottom left corner.
        variant : str, optional
            The variant with how to fill the key square (default: clockwise)
            Supported values are clockwise and counter
        pad : str, optional
            The type of padding to apply to the keyword (default: standard)
            Supported values are standard and remainder.
            Standard padding follows the alphabet starting from "a".
            Remainder padding follows the alphabet starting from the last
            letter of the keyword.
    """
    # Ensure valid parameters are passed to the key setup
    check_key(start, variant, pad)
    if variant in ("row", "column"):
        sys.stderr.write("Error: Spiral keys don't support row and column variants")
        sys.exit(4)

    square = {
        0: ["", "", "", "", ""],
        1: ["", "", "", "", ""],
        2: ["", "", "", "", ""],
        3: ["", "", "", "", ""],
        4: ["", "", "", "", ""],
    }

    new_phrase = setup_keyword(phrase)
    letters = new_phrase + get_padding(new_phrase, pad)

    index = 0
    clock = [1, 1, -1, -1]
    if start == 0:
        coords = [0, 0]
    elif start == 1:
        coords = [0, 4]
    elif start == 2:
        coords = [4, 4]
    else:
        coords = [4, 0]
    # Determine the direction of the spiral
    if variant == "clockwise":
        for _ in range(0, 9):
            if start in (0, 2):
                index += fill_row(square, coords, letters[index:], clock[start])
                coords[0] += clock[start]
            else:
                index += fill_col(square, coords, letters[index:], clock[start])
                coords[1] -= clock[start]
            start = (start + 1) % 4
    else:
        # The starting direction for [0, 4] and [4, 0] are flipped and don't
        # index into the clock array correctly. Swap their directions as the
        # coodinates are already set
        if start == 1:
            start = 3
        elif start == 3:
            start = 1
        for _ in range(0, 9):
            if start in (0, 2):
                index += fill_col(square, coords, letters[index:], clock[start])
                coords[1] += clock[start]
            else:
                index += fill_row(square, coords, letters[index:], clock[start])
                coords[0] -= clock[start]
            start = (start + 1) % 4

    return "".join(["".join(x) for x in square.values()])


def regular_key(phrase, start, variant="row", pad="standard"):
    """Set up a regular key

    Arguments:
        phrase : str
            Phrase to use in the key square
        start : int, optional
            The starting position in the key square (default: 0)
            Supported values are 0, 1, 2, and 3. These correspond to the
            four corners of the key square.  0 refers to the top left
            corner, 1 refers to the top right corner, 2 refers to the
            bottom right corner, and 3 refers to the bottom left corner.
        variant : str, optional
            The variant with how to fill the key square (default: row)
            Supported values are row and column
        pad : str, optional
            The type of padding to apply to the keyword (default: standard)
            Supported values are standard and remainder.
            Standard padding follows the alphabet starting from "a".
            Remainder padding follows the alphabet starting from the last
            letter of the keyword.
    """
    # Ensure valid parameters are passed to the key setup
    check_key(start, variant, pad)
    if variant in ("clockwise", "counter"):
        sys.stderr.write(
            "Error: Spiral keys don't support clockwise and counter variants"
        )
        sys.exit(4)

    square = {
        0: ["", "", "", "", ""],
        1: ["", "", "", "", ""],
        2: ["", "", "", "", ""],
        3: ["", "", "", "", ""],
        4: ["", "", "", "", ""],
    }

    new_phrase = setup_keyword(phrase)
    letters = new_phrase + get_padding(new_phrase, pad)

    index = 0
    if start == 0:
        if variant == "row":
            for i in range(0, 5):
                index += fill_row(square, [i, 0], letters[index:], 1)
        else:
            for i in range(0, 5):
                index += fill_col(square, [0, i], letters[index:], 1)
    elif start == 1:
        if variant == "row":
            for i in range(0, 5):
                index += fill_row(square, [i, 4], letters[index:], -1)
        else:
            for i in range(0, 5):
                index += fill_col(square, [0, 4 - i], letters[index:], 1)
    elif start == 2:
        if variant == "row":
            for i in range(0, 5):
                index += fill_row(square, [4 - i, 4], letters[index:], -1)
        else:
            for i in range(0, 5):
                index += fill_col(square, [4, 4 - i], letters[index:], -1)
    elif start == 3:
        if variant == "row":
            for i in range(0, 5):
                index += fill_row(square, [4 - i, 0], letters[index:], 1)
        else:
            for i in range(0, 5):
                index += fill_col(square, [4, i], letters[index:], -1)

    return "".join(["".join(x) for x in square.values()])


class Playfair:
    """Class used for the Playfair cipher

    Attributes:
        key : str
            Keyword or keyphrase to use in the key square
        row : int, optional
            number of rows in the key square (default: 5)
        col : int, optional
            number of columns in the key square (default: 5)

    Methods:
        encrypt(plaintext)
            Encrypt the plaintext using the playfair algorithm
        decrypt(ciphertext)
            Decrypt the ciphertext using the playfair algorithm
    """

    def __init__(self, keyword, start=0, variant="row", pad="standard"):
        """Initialization function for class

        Arguments:
            keyword : str
                Keyword or keyphrase to use in the key square
            start : int, optional
                The starting position in the key square (default: 0)
                Supported values are 0, 1, 2, and 3. These correspond to the
                four corners of the key square.  0 refers to the top left
                corner, 1 refers to the top right corner, 2 refers to the
                bottom right corner, and 3 refers to the bottom left corner.
            variant : str, optional
                The variant with how to fill the key square (default: row)
                Supported values are row, column, clockwise, counter
            pad : str, optional
                The type of padding to apply to the keyword (default: standard)
                Supported values are standard and remainder.
                Standard padding follows the alphabet starting from "a".
                Remainder padding follows the alphabet starting from the last
                letter of the keyword.
        """
        # At the moment, we only support 5x5 key squares
        self.row = 5
        self.col = 5
        if variant in ("row", "column"):
            self.key = regular_key(keyword, start, variant, pad)
        else:
            self.key = spiral_key(keyword, start, variant, pad)

    def encrypt(self, plaintext):
        """Encrypt the plaintext using the playfair algorithm

        Arguments:
            plaintext : str
                Text that we want to encrypt. Spaces, newlines, and any non
                alpha characters will be stripped automatically. Case
                insensitive.

        Returns:
            ciphertext : str
                Encrypted plaintext
        """
        # Ensure no spaces or newlines in the plaintext
        text = "".join("".join(plaintext.split(" ")).split("\n")).lower()
        # Strip out anything that is not a letter
        text = "".join([x for x in text if x in string.ascii_lowercase])

        # If using a 5x5 key square, we replace "j" with "i" for simplicity
        if (self.row * self.col) == 25:
            text = text.replace("j", "i")

        padded_text = ""
        padding = 0
        for i, letter in enumerate(text):
            padded_text += letter
            # Add padding if the next letter is the same and if we are at an
            # even boundary; Skip padding if we are at the last letter, since
            # we can't check the next letter
            if (
                i != (len(text) - 1)
                and text[i + 1] == letter
                and (i + padding) % 2 == 0
            ):
                padded_text += "x"
                padding += 1
        # Add padding to the end of string if we have an odd length
        if len(padded_text) % 2:
            padded_text += "x"

        ciphertext = ""
        for i in range(0, len(padded_text), 2):
            p1 = padded_text[i]
            p1_index = self.key.index(p1)
            p2 = padded_text[i + 1]
            p2_index = self.key.index(p2)

            if p1_index // self.row == p2_index // self.row:
                # If letters are in the same row, we choose the next letter in
                # the same row and wrap to the beginning of the row if needed
                row_list = [
                    x
                    for x in range(self.row * self.col)
                    if x // self.row == p1_index // self.row
                ]
                ciphertext += self.key[
                    row_list[(row_list.index(p1_index) + 1) % self.row]
                ]
                ciphertext += self.key[
                    row_list[(row_list.index(p2_index) + 1) % self.row]
                ]
            elif p1_index % self.col == p2_index % self.col:
                # If letters are in the same column, we choose the next letter
                # in the same column and wrap to the top of the column if needed
                col_list = [
                    x
                    for x in range(self.row * self.col)
                    if x % self.col == p1_index % self.col
                ]
                ciphertext += self.key[
                    col_list[(col_list.index(p1_index) + 1) % self.col]
                ]
                ciphertext += self.key[
                    col_list[(col_list.index(p2_index) + 1) % self.col]
                ]
            else:
                # If not in the same column or row, we form opposite corners
                # in the key square with the letters and choose the cipher text
                # based on the remaining corners
                ciphertext += self.key[
                    get_vertex(p1_index, p2_index, self.row, self.col)
                ]
                ciphertext += self.key[
                    get_vertex(p2_index, p1_index, self.row, self.col)
                ]

        return ciphertext

    def decrypt(self, ciphertext):
        """Decrypt the ciphertext using the playfair algorithm

        Arguments:
            ciphertext : str
                Text that we want to decrypt. Spaces, newlines, and any non
                alpha characters will be stripped automatically. Case
                insensitive.

        Returns:
            plaintext : str
                Decrypted ciphertext
        """
        # Ensure no spaces or newlines in the ciphertext
        text = "".join("".join(ciphertext.split(" ")).split("\n")).lower()
        # Strip out anything that is not a letter
        text = "".join([x for x in text if x in string.ascii_lowercase])

        plaintext = ""
        for i in range(0, len(text), 2):
            c1 = text[i]
            c1_index = self.key.index(c1)
            c2 = text[i + 1]
            c2_index = self.key.index(c2)

            if c1_index // self.row == c2_index // self.row:
                # If letters are in the same row, we choose the next letter in
                # the same row and wrap to the beginning of the row if needed
                row_list = [
                    x
                    for x in range(self.row * self.col)
                    if x // self.row == c1_index // self.row
                ]
                plaintext += self.key[
                    row_list[(row_list.index(c1_index) - 1) % self.row]
                ]
                plaintext += self.key[
                    row_list[(row_list.index(c2_index) - 1) % self.row]
                ]
            elif c1_index % self.col == c2_index % self.col:
                # If letters are in the same column, we choose the next letter
                # in the same column and wrap to the top of the column if needed
                col_list = [
                    x
                    for x in range(self.row * self.col)
                    if x % self.col == c1_index % self.col
                ]
                plaintext += self.key[
                    col_list[(col_list.index(c1_index) - 1) % self.col]
                ]
                plaintext += self.key[
                    col_list[(col_list.index(c2_index) - 1) % self.col]
                ]
            else:
                # If not in the same column or row, we form opposite corners
                # in the key square with the letters and choose the cipher text
                # based on the remaining corners
                plaintext += self.key[
                    get_vertex(c1_index, c2_index, self.row, self.col)
                ]
                plaintext += self.key[
                    get_vertex(c2_index, c1_index, self.row, self.col)
                ]

        return plaintext
