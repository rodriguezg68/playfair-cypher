#!/usr/bin/env python3
"""Try to decode a specific message"""
from playfair import Playfair, regular_key, spiral_key

MESSAGE = """
AWHGS EHONR TXZLF ZNHTE LZBSI

WHOLZ HDBSS NWEET EAQTW EILGY

WDRTV TRZAR UVKGS XFVAR LYEDT

XWKZN ARUIR TESXO LZNGM YSELM

FN
"""


def main():
    """Entry Point"""
    key_list = []
    # Setup the key list by going through all variants of a 5x5 key square
    for i in range(0, 4):
        for padding in ("standard", "remainder"):
            for variant in ("row", "column"):
                key_list.append(
                    regular_key("quizmaster", i, variant=variant, pad=padding)
                )
            for variant in ("clockwise", "counter"):
                key_list.append(
                    spiral_key("quizmaster", i, variant=variant, pad=padding)
                )

    correct_index = 0
    answer = ""

    for i, key in enumerate(key_list):
        engine = Playfair(key)
        plaintext = engine.decrypt(MESSAGE)
        print(i, plaintext)
        if "ptboatone" in plaintext:
            correct_index = i
            answer = plaintext

    print("\nKey: " + key_list[correct_index])
    print("Index: " + str(correct_index))
    print("Decrypted message:\n" + answer)


if __name__ == "__main__":
    main()
